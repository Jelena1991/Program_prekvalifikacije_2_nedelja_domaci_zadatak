# Program prekvalifikacija - 2. nedelja

### Domaći zadatak:
  - Kreirati stranicu koja poseduje jedan obrazac ali sa dva izdvojena dela (personal i hobi, koristiti fieldset). 
  - Obavezno treba da se u obrascu koriste pored tekstualnih polja radio tasteri, checkboxovi, select liste sa grupisanjem, tasteri.        Ispod obrasca ubaciti u tabelu dva video fajla sa youtube-a. 
  - Koristiti meta tagove.  Strukturu stranice uraditi preko kontejnerskih elemenata. 
  - Poseban naglasak obratiti na pravilno definisanje atributa kod svih polja (ime, vrednosti) kao i na zaglavlje obrasca. 
  - Podaci se šalju POST metodom na stranicu register.php
